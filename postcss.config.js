module.exports = {
  plugins: {
    'postcss-preset-env': {},
    'cssnano': {
      preset: [
        'advanced', {
          discardComments: { removeAll: true },
          zindex: false,
          autoprefixer: {
            browsers: ['last 1 versions', '> 1%'],
            add: true,
            remove: false
          }
        }
      ]
    }
  }
}