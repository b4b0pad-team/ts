export interface BaseController {
    inits(): Promise<any>[];
    handleEvents(): void;
}