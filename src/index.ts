import {site_type as Site} from 'CONTROLLERS/Site';
import {router_type as Router} from 'SERVICES/system/Router';

const init_site = async () => {
  let site: Site = new (await import('CONTROLLERS/Site' /* webpackChunkName: 'Site' */)).default();
  let async_inits: Promise<any>[] = site.inits();
  await Promise.all(async_inits);
  site.handleEvents();
}

const load_controller = async () => {
  let router: Router = (await import('SERVICES/system/Router' /* webpackChunkName: 'Router' */)).default;
  router.loadPageController();
}

init_site();
load_controller();