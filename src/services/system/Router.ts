import {BaseController} from 'INTERFACES/BaseController';

class Router {
  private static instance: Router;
  private body: JQuery;

  private constructor() {
    this.body = $('body');
  }

  static getInstance(): Router {
    if (!Router.instance) {
      Router.instance = new Router();
    }

    return Router.instance;
  }

  getCurrentPage(): string {
    return this.body.data('page') || 'default';
  }

  async loadPageController(): Promise<Boolean> {
    let controller: BaseController;
    const controller_name: String = this.getCurrentPage();
    switch (controller_name) {
      case 'homepage':
        controller = new (await import('CONTROLLERS/Homepage'  /* webpackChunkName: 'Homepage' */)).default();
        break;
      default:
        return false;
        break;
    }
    let async_inits: Promise<any>[] = controller.inits();
    await Promise.all(async_inits);
    controller.handleEvents();
    return true;
  }
}

const router: Router = Router.getInstance();

export default router;
export type router_type = typeof router;