import config_file from 'NODE_MODULES/../project.config.json';

class Framework {
  private static instance: Framework;
  private page: JQuery;
  private readonly configurations: any = config_file;
  options: {
    IS_DEBUG: boolean;
  };

  private constructor() {
    this.page = jQuery('<div></div>');
    this.options = {
      IS_DEBUG: this.configurations.DEBUG
    };
  }

  static getInstance(): Framework {
    if (!Framework.instance) {
      Framework.instance = new Framework();
    }

    return Framework.instance;
  }

  notify(a_event_name: string, a_data: any = {}): void {
    if (this.options.IS_DEBUG) {
      console.info(a_event_name, a_data);
    }
    this.page.trigger(a_event_name);
  }

  watch(
    a_event_name: string,
    a_handler: any,
    is_one_time: boolean = false
  ): void {
    if (is_one_time) {
      this.page.one(a_event_name, null, null, a_handler);
    } else {
      this.page.on(a_event_name, null, null, a_handler);
    }
  }
}

const framework: Framework = Framework.getInstance();

export default framework;
export type framework_type = typeof framework;