class Device {
  private static instance: Device;
  private _element_to_define: JQuery;
  private _current_type: string;
  private _user_agent: string;
  private readonly TYPE_SMARTPHONE: string = 'smartphone';
  private readonly TYPE_TABLET: string = 'tablet';
  private readonly TYPE_DESKTOP: string = 'desktop';

  private constructor() {
    this._element_to_define = $('body');
    this._user_agent = navigator.userAgent;
  }

  static getInstance(): Device {
    if (!Device.instance) {
      Device.instance = new Device();
    }
    return Device.instance;
  }

  checkAndSet(): void {
    this._element_to_define.removeClass(
      `${this.TYPE_SMARTPHONE} ${this.TYPE_TABLET} ${this.TYPE_DESKTOP}`
    );

    if (/Tablet|iPad/i.test(this._user_agent)) {
      this._element_to_define.addClass(this.TYPE_TABLET);
    } else if (
      /IEMobile|Windows Phone|Lumia/i.test(this._user_agent) ||
      /iPhone|iP[oa]d/.test(this._user_agent) ||
      /Android/.test(this._user_agent) ||
      /BlackBerry|PlayBook|BB10/.test(this._user_agent) ||
      /Mobile Safari/.test(this._user_agent) ||
      /webOS|Mobile|Tablet|Opera Mini|\bCrMo\/|Opera Mobi/i.test(
        this._user_agent
      )
    ) {
      this._element_to_define.addClass(this.TYPE_SMARTPHONE);
    } else if (
      /Edge\/\d+/.test(this._user_agent) ||
      /MSIE 9/.test(this._user_agent) ||
      /MSIE 10/.test(this._user_agent) ||
      /MSIE 11/.test(this._user_agent) ||
      /MSIE\s\d/.test(this._user_agent) ||
      /rv\:11/.test(this._user_agent) ||
      /Firefox\W\d/.test(this._user_agent) ||
      /Chrome\W\d/.test(this._user_agent) ||
      /Chromium\W\d/.test(this._user_agent) ||
      /\bSafari\W\d/.test(this._user_agent) ||
      /\bOpera\W\d/.test(this._user_agent) ||
      /\bOPR\W\d/i.test(this._user_agent) ||
      typeof MSPointerEvent !== 'undefined'
    ) {
      this._element_to_define.addClass(this.TYPE_DESKTOP);
    }

    this._current_type = this.getType();
  }

  isDesktop(): boolean {
    return this._element_to_define.hasClass(this.TYPE_DESKTOP);
  }

  isTablet(): boolean {
    return this._element_to_define.hasClass(this.TYPE_TABLET);
  }

  isSmartphone(): boolean {
    return this._element_to_define.hasClass(this.TYPE_SMARTPHONE);
  }

  isPortrait(): boolean {
    return window.innerHeight > window.innerWidth;
  }

  isLandscape(): boolean {
    return !this.isPortrait();
  }

  getType(): string {
    if (this.isDesktop()) {
      return this.TYPE_DESKTOP;
    }

    if (this.isTablet()) {
      return this.TYPE_TABLET;
    }

    if (this.isSmartphone()) {
      return this.TYPE_SMARTPHONE;
    }
  }

  isWebKit(): boolean {
    return this.isChrome() || this.isSafari();
  }

  isChrome(): boolean {
    let isChrome = /Chrome/.test(this._user_agent);
    return isChrome;
  }

  isSafari(): boolean {
    let isSafari = /Safari/.test(this._user_agent);
    return isSafari;
  }

  isAndroid(): boolean {
    let isAndroid = this._user_agent.toLowerCase().indexOf('android') > -1;
    return isAndroid;
  }
}

const device: Device = Device.getInstance();

export default device;
export type device_type = typeof device;