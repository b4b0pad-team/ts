import {BaseController} from 'INTERFACES/BaseController';
import {framework_type as Framework} from 'SERVICES/system/Framework';
import {device_type as Device} from 'SERVICES/devices/Device';

import 'PAGES/site.scss';

class Site implements BaseController {
  private framework: Framework;
  private device: Device;
  
  constructor() {
    this._init();
  }

  private _init() {
    console.info('SITE INIT DONE')
  }

  private async _getFramework() {
    if (typeof this.framework === 'undefined') {
      this.framework = (await import('SERVICES/system/Framework' /* webpackChunkName: 'Framework' */)).default;
    }
    return this.framework;
  }

  async _getDevice() {
    if (typeof this.device === 'undefined') {
      this.device = (await import('SERVICES/devices/Device' /* webpackChunkName: 'Device' */)).default;
    }
    return this.device;
  }
  
  async setDevice(): Promise<any> {
    (await this._getDevice()).checkAndSet();
  }
  
  inits(): Promise<any>[] {
    return [
      this.setDevice()
    ];
  }

  async handleEvents() {
    let framework: Framework = (await this._getFramework());
    // ...do something
    framework.notify('SYSTEM::SITE::HANDLE_EVENTS_DONE');
  }
}

export default Site;
export type site_type = Site;