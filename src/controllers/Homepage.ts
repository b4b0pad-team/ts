import {BaseController} from 'INTERFACES/BaseController';
import {framework_type as Framework} from 'SERVICES/system/Framework';

import 'PAGES/homepage.scss';

class Homepage implements BaseController {
  private framework: Framework;
  
  constructor() {
    this._init();
  }

  private async _getFramework() {
    if (typeof this.framework === 'undefined') {
      this.framework = (await import('SERVICES/system/Framework' /* webpackChunkName: 'Framework' */)).default;
    }
    return this.framework;
  }

  private _init() {
//     $('app').append('<span class="icon icon-user"></span>');
    console.info('HOMEPAGE INIT DONE')
  }

  async asyncMethod() {
    (await this._getFramework()).notify('SYSTEM::HOMEPAGE::ASYNC_METHOD_DONE');
  }
  
  inits(): Promise<any>[] {
    return [this.asyncMethod()];
  }

  async handleEvents() {
    (await this._getFramework()).notify('SYSTEM::HOMEPAGE::HANDLE_EVENTS_DONE');
  }
}

export default Homepage;