const path = require('path');
const webpack = require('webpack');
const argv = require('yargs').argv;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin  = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const modules_path = path.join(__dirname, 'node_modules');
const dev_scripts_path = path.join(__dirname, 'src');
const dest_scripts_path = path.join(__dirname, 'dist');

const critical_file_index = 'index.html';
// const critical_file_index = 'header.php';
const critical_file_style = 'critical.scss';
const critical_file_js = 'index.ts';

const IS_PRODUCTION = typeof argv.env !== 'undefined' && argv.env.toLowerCase() === 'release';

module.exports = {
  entry: [
    path.resolve(__dirname, dev_scripts_path, critical_file_js),
    path.resolve(__dirname, dev_scripts_path, critical_file_style)
  ],
  output: {
    filename: '[name].js',
    path: dest_scripts_path,
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'awesome-typescript-loader',
        options: {
          configFileName: path.join(__dirname, '/tsconfig.json')
        }
      },
      {
        test: /\.scss$/,
        exclude: [
          path.resolve(__dirname, dev_scripts_path, critical_file_style)
        ],
        use: [
          {
            loader: "style-loader"
          }, {
            loader: "css-loader",
            options: {
              sourceMap: true,
              importLoaders: 1
            }
          },
          { 
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: path.resolve(__dirname, dev_scripts_path, critical_file_style),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
              importLoaders: 1
            }
          },
          { 
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }          
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  },
  devtool: 'source-map',
  externals: {
    win: 'window'
  },
  mode: IS_PRODUCTION ? 'production' : 'development',
  resolve: {
    alias: {
      NODE_MODULES: modules_path,
      COMPONENTS: path.join(dev_scripts_path, 'components'),
      CONTROLLERS: path.join(dev_scripts_path, 'controllers'),
      INTERFACES: path.join(dev_scripts_path, 'interfaces'),
      SERVICES: path.join(dev_scripts_path, 'services'),
      PAGES: path.join(dev_scripts_path, 'pages')
    },
    modules: [modules_path],
    extensions: ['.tsx', '.ts', '.js', '.jsx']
  },
  resolveLoader: {
    modules: [modules_path]
  },
  optimization: {
    minimize: IS_PRODUCTION,
    runtimeChunk: false,
    mangleWasmImports: IS_PRODUCTION,
    removeAvailableModules: IS_PRODUCTION,
    removeEmptyChunks: IS_PRODUCTION,
    mergeDuplicateChunks: IS_PRODUCTION,
    flagIncludedChunks: IS_PRODUCTION,
    occurrenceOrder: IS_PRODUCTION,
    providedExports: IS_PRODUCTION,
    usedExports: IS_PRODUCTION,
    concatenateModules: IS_PRODUCTION,
    sideEffects: IS_PRODUCTION
  },
  plugins: [
    // ALL ENVS
    new CleanWebpackPlugin([dest_scripts_path]),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new HtmlWebpackPlugin({
      filename: path.join(__dirname, critical_file_index),
      template: path.join(__dirname, 'webpack_templates', critical_file_index),
      inject: 'head',
      inlineSource: '.(js|css)$'
    }),
    new HtmlWebpackInlineSourcePlugin()
  ].concat(
    IS_PRODUCTION ? [
          // ONLY PROD
        ]
      : [
          // ONLY DEV
        ]
  )
}